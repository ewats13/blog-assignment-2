class Comment < ActiveRecord::Base
  belongs_to :post
  validates :post, :body, presence: true
end
